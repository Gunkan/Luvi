
/**
 *
 *! \file        FiscalPrinter.c
 * \brief
 * \date	18 March 2017
 * \n This file contains utility function for formating buffer at the TLV format
 * \n
 * \note definition will be added later on
 * \link
 * \example
 * \author cemal.gunkan@ingenico.com
 * \copyright Ingenico TR (R&D).
**/

#include "string.h"
#include "luainc.h"
#include "GMPSmartDLL.h"

#define LUA_INTERNAL_NAME			"payment.trans"
#define LUA_FISCAL_LIBRARY_NAME		"Fiscal"

// extern function
extern void LongToBcd(int pSrc, uint8 *pOutDest, int pDestLen);
extern void *umalloc(unsigned int size);
extern void ufree(void*p);


// static function
static void Parse_SetInvoice(lua_State *L, ST_INVIOCE_INFO *pstInvoiceInfo);
static void Parse_SaleItem(lua_State *L, ST_ITEM *pstItem);
static void Parse_Payment(lua_State *L,  ST_PAYMENT_REQUEST *pstPaymentRequest);


typedef struct
{
	unsigned long long 	handle;
	unsigned long		retcode;
	char 				*pUniqueId;
}ST_Handle;

ST_Handle xHandle;

/**	\brief	The trace function
 *
 * @param L
 * @return 0
 * \n
 * \n debug utility for the lua code on the C side..
 *
 */
static int trace(lua_State *L)
{
	const char* str;
	char* p;
	/* get number of arguments */
	int n = lua_gettop(L);

	str = lua_tostring(L, 1);
	 n = strlen(str);
	p = (char*)umalloc(n + 4);
	strcpy(p, str);
	p[n++] = '\n';
	p[n] = 0;

	//TRACEX(p);
	ufree(p);
	return 0;
}

/**	\brief	The check_Obj function
 *
 * @param L
 * @param i
 * @return *handle pointer
 *
 * \n
 * \n Check the userdata gmp3 handle initialized
 */

ST_Handle *check_Obj(lua_State *L, int i)
{
	ST_Handle *pxHandle;

	pxHandle  = (ST_Handle *)luaL_checkudata(L, i, LUA_INTERNAL_NAME);
	return pxHandle;
}

/**	\brief	The ingenico__gc function
 *
 * @param L
 * @return 0
 *
 * \n
 * \n destruction of the user data, called end of the object life cycle
 */
int ingenico__gc(lua_State *L)
{
	ST_Handle *pxHandle;

	pxHandle = check_Obj(L, 1);
	if (pxHandle->pUniqueId)
	{
		ufree(pxHandle->pUniqueId);
	}
	return 0;
}
///////////////////////////////////Start Fiscal functions ///////////////////////////////////////////

/**	\brief	The Lua_FiscalPrinter_Pretotal function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_Pretotal(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	ST_TICKET m_stTicket;

	memset(&m_stTicket, 0x00, sizeof(m_stTicket));

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	retcode = FiscalPrinter_Pretotal(&m_stTicket, 0 );


	//push the result code
	lua_pushnumber(L, retcode);

	//push the receipt amount
	lua_pushinteger(L, m_stTicket.TotalReceiptAmount);

	/* return the number of results */
	return 2;
}

/**	\brief	The Lua_FiscalPrinter_VoidItem function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_VoidItem(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	ST_TICKET m_stTicket;
	int		n, i;
	int		indexOfItem, itemCount, itemCountPrecision;


	// params : object, indexOfItem, itemCount, itemCountPrecision
	memset(&m_stTicket, 0x00, sizeof(m_stTicket));

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 4) // including userdata
		retcode = APP_ERR_MISSING_PARAMETER;

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:
					break;
				case LUA_TBOOLEAN:
					break;
				case LUA_TNUMBER:
					if (i == 2)
					{
						indexOfItem = lua_tonumber(L, i) - 1;
						indexOfItem = indexOfItem>=512?0xFFFF:indexOfItem;
					}
					else if (i == 3)
						itemCount = lua_tonumber(L, i) ;
					else if (i == 4)
						itemCountPrecision = lua_tonumber(L, i) ;
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		retcode = FiscalPrinter_VoidItem(indexOfItem, itemCount, itemCountPrecision, &m_stTicket, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}


/**	\brief	The Lua_FiscalPrinter_Inc function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_Inc(lua_State *L)
{
	ST_TICKET stTicket;
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	uint32 changedAmount;
	int		n, i;
	char    msg[128];
	int		rate;
	int		indexOfItem;

	// parameters : rate, text, indexOfItem
	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 4) // including userdata
		retcode = APP_ERR_MISSING_PARAMETER;

	memset(&stTicket, 0x00, sizeof(stTicket));

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:
					strcpy(msg, lua_tostring(L, i));
					break;
				case LUA_TBOOLEAN:
					//lua_toboolean(L, i) ? "true" : "false");
					break;
				case LUA_TNUMBER:
					if (i == 2)
						rate = lua_tonumber(L, i);
					else if (i == 4)
					{
						indexOfItem = lua_tonumber(L, i) - 1;
						indexOfItem = indexOfItem>=512?0xFFFF:indexOfItem;
					}
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		retcode = FiscalPrinter_Inc_Ex(rate, msg, &stTicket, indexOfItem, &changedAmount, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}

/**	\brief	The Lua_FiscalPrinter_Minus function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_Minus(lua_State *L)
{
	ST_TICKET stTicket;
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	int		n, i;
	char    msg[128];
	int		amount;
	int		indexOfItem;

	// parameters : amount, text, indexOfItem
	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 4) // including userdata
		retcode = APP_ERR_MISSING_PARAMETER;

	memset(&stTicket, 0x00, sizeof(stTicket));

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:
					strcpy(msg, lua_tostring(L, i));
					break;
				case LUA_TBOOLEAN:
					//lua_toboolean(L, i) ? "true" : "false");
					break;
				case LUA_TNUMBER:
					if (i == 2)
						amount = lua_tonumber(L, i);
					else if (i == 4)
					{
						indexOfItem = lua_tonumber(L, i) - 1;
						indexOfItem = indexOfItem>=512?0xFFFF:indexOfItem;
					}
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		//retcode = FiscalPrinter_Minus_Ex( amount , Text, &m_stTicket, itemNoDlg.m_iItemNo ))
		retcode = FiscalPrinter_Minus_Ex(amount, msg, &stTicket, indexOfItem, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}


/**	\brief	The Lua_FiscalPrinter_Dec function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_Dec(lua_State *L)
{
	ST_TICKET stTicket;
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	uint32 changedAmount;
	int		n, i;
	char    msg[128];
	int		rate;
	int		indexOfItem;

	// parameters : rate, text, indexOfItem
	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 4) // including userdata
		retcode = APP_ERR_MISSING_PARAMETER;

	memset(&stTicket, 0x00, sizeof(stTicket));

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:
					strcpy(msg, lua_tostring(L, i));
					break;
				case LUA_TBOOLEAN:
					//lua_toboolean(L, i) ? "true" : "false");
					break;
				case LUA_TNUMBER:
					if (i == 2)
						rate = lua_tonumber(L, i);
					else if (i == 4)
					{
						indexOfItem = lua_tonumber(L, i) - 1;
						indexOfItem = indexOfItem>=512?0xFFFF:indexOfItem;
					}
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		retcode = FiscalPrinter_Dec_Ex(rate, msg, &stTicket, indexOfItem, &changedAmount, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}

/**	\brief	The Lua_FiscalPrinter_GetTicket function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_GetTicket(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	ST_TICKET m_stTicket;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	memset( &m_stTicket, 0x00, sizeof(ST_TICKET));

	while (retcode == TRAN_RESULT_OK)
	{
		retcode = FiscalPrinter_GetTicket( &m_stTicket, 0 );
		if( retcode )
			return retcode;

		if( m_stTicket.totalNumberOfItems > m_stTicket.numberOfItemsInThis )
			continue;

		if( m_stTicket.totalNumberOfPayments > m_stTicket.numberOfPaymentsInThis )
			continue;

		//if( m_stTicket.totalNumberOfPrinterLines > m_stTicket.numberOfPrinterLinesInThis )
		//	continue;

		// T�m item ve printer sat�rlar� geldi
		break;
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}

/**	\brief	The Lua_FiscalPrinter_OptionFlags function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_OptionFlags(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	uint64	activeFlags=0;
	uint64	flagTobeSet=0;
	uint64	flagTobeClear=0;
	int		n, i;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 3)
		retcode = APP_ERR_MISSING_PARAMETER;

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:
					//lua_tostring(L, i));
					break;
				case LUA_TBOOLEAN:
					//lua_toboolean(L, i) ? "true" : "false");
					break;
				case LUA_TNUMBER:
					if (i == 2)
						flagTobeSet = lua_tonumber(L, i);
					else if (i == 3)
						flagTobeClear = lua_tonumber(L, i);
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		retcode = FiscalPrinter_OptionFlags( &activeFlags,  flagTobeSet, flagTobeClear, 0 );
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}

/**	\brief	The Lua_FiscalPrinter_PrintUserMessage function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_PrintUserMessage(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	ST_USER_MESSAGE stUserMessage[2];
	ST_TICKET m_stTicket;
	int		n, i;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);
	if (n != 3)
		retcode = APP_ERR_MISSING_PARAMETER;

	memset(&m_stTicket, 0x00, sizeof(m_stTicket));
	memset(&stUserMessage, 0x00, sizeof(stUserMessage));

	if (retcode == 0)
	{
		for (i = 1; i <= n; i++)
		{
			int t = lua_type(L, i);
			switch (t)
			{
				case LUA_TSTRING:

					strcpy( (char*)stUserMessage[0].message, lua_tostring(L, i));
					stUserMessage[0].len = strlen((char*)stUserMessage[0].message);
					break;
				case LUA_TBOOLEAN:
					//lua_toboolean(L, i) ? "true" : "false");
					break;
				case LUA_TNUMBER:
					stUserMessage[0].flag = lua_tonumber(L, i);
					break;
				case LUA_TUSERDATA:
					break;
				default:  /* other values */
					break;
			}
		}
		retcode = FiscalPrinter_PrintUserMessage(stUserMessage, 1,  &m_stTicket, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}


/**	\brief	The Lua_FiscalPrinter_PrintTotalsAndPayments function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_PrintTotalsAndPayments(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	retcode = FiscalPrinter_PrintTotalsAndPayments(0);

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}

/**	\brief	The Lua_FiscalPrinter_PrintBeforeMF function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_PrintBeforeMF(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	retcode = FiscalPrinter_PrintBeforeMF(0);

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}

/**	\brief	The Lua_FiscalPrinter_PrintMF function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */

int Lua_FiscalPrinter_PrintMF(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	retcode = FiscalPrinter_PrintMF(0);

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}

/**	\brief	The Lua_FiscalPrinter_Close function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */
int Lua_FiscalPrinter_Close(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	retcode = FiscalPrinter_Close(0);

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}

/**	\brief	The Lua_FiscalPrinter_Payment function
 *
 * @param L
 * @return 0 on success, otherwise see header file for other value.
 */
ST_TICKET 			stTicket;
ST_PAYMENT_REQUEST 	stPaymentRequest;

int Lua_FiscalPrinter_Payment(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	int i, n = lua_gettop(L);

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
	{
		retcode = APP_ERR_GMP3_NO_HANDLE;
		goto Exit;
	}
	for ( i = 1 ; i <= n ; ++i)
	{
		const char *p;
		p = lua_typename(L, i);
		if (lua_istable(L, i))
		{
			Parse_Payment(L, &stPaymentRequest);
		}
	}

	memset(&stTicket, 0x00, sizeof(stTicket));
	retcode = FiscalPrinter_Payment( &stPaymentRequest, &stTicket, 0);
	//retcode = FiscalPrinter_Payment( &stPaymentRequest, NULL, 0);

	lua_createtable(L, 2, 0);
	lua_pushstring(L, "Remain");
	//lua_pushinteger(L, stTicket.TotalReceiptAmount - stTicket.TotalReceiptPayment);
	lua_pushinteger(L, 0);
	lua_settable(L, -3);

	lua_pushstring(L, "ReceiptAmount");
	//lua_pushinteger(L, stTicket.TotalReceiptAmount);
	lua_pushinteger(L, 10);
	lua_settable(L, -3);

	lua_setglobal(L, "paymentResp");

	Exit:
	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}
/**	\brief	The Parse_Payment function
 *
 * @param L
 * @param pstPaymentRequest
 */
static void Parse_Payment(lua_State *L, ST_PAYMENT_REQUEST *pstPaymentRequest)
{

	memset( pstPaymentRequest, 0, sizeof(ST_PAYMENT_REQUEST));

	lua_pushnil(L);

	while(lua_next(L, -2) != 0)
    {
        if(lua_isstring(L, -1))
		{
			//printf("%s = %s\n", lua_tostring(L, -2), lua_tostring(L, -1));
			if (!memcmp(lua_tostring(L, -2), "typeOfPayment", strlen("typeOfPayment")))
			{
				pstPaymentRequest->typeOfPayment = (uint32)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "payAmountBonus", 14))
			{
				pstPaymentRequest->payAmountBonus = (uint32)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "payAmount", 9))
			{
				pstPaymentRequest->payAmount = (uint32)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "currency", strlen("currency")))
			{
				pstPaymentRequest->payAmountCurrencyCode = (uint16)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "bankBkmId", strlen("bankBkmId")))
			{
				pstPaymentRequest->bankBkmId = (uint16)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "numberOfinstallments", strlen("numberOfinstallments")))
			{
				pstPaymentRequest->numberOfinstallments = (uint16)lua_tonumber(L, -1);
			}
			else if(!memcmp(lua_tostring(L, -2), "transactionFlag", strlen("transactionFlag")))
			{
				pstPaymentRequest->transactionFlag = (uint32)lua_tonumber(L, -1);
			}
		}
        else if(lua_isnumber(L, -1))
		{
          //printf("%s = %d\n", lua_tostring(L, -2), lua_tonumber(L, -1));
		}
        else if(lua_istable(L, -1))
		{
			break;
		}
        lua_pop(L, 1);

    }
}



/**	\brief	The Lua_FiscalPrinter_ItemSale function
 *
 * @param L
 * @return
 * \n
 * \n Process the parsed item then send message to gmp3 library for the sale item
 */
int Lua_FiscalPrinter_ItemSale(lua_State *L)
{
	ST_ITEM stItem;

	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	int i, n = lua_gettop(L);

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;
	if (retcode == 0)
	{
		memset(&stItem, 0x00, sizeof(stItem));
		for ( i = 1 ; i <= n ; ++i)
		{
			const char *p;
			p = lua_typename(L, i);
			if (lua_istable(L, i))
			{
				Parse_SaleItem(L, &stItem);
			}
		}
		retcode = FiscalPrinter_ItemSale(&stItem, NULL, 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;

}
/**	\brief	The Lua_FiscalPrinter_ItemSale function
 *
 * @param L
 * @param pstItem
 * \n
 * \n Parse the item from lua script and fill the pstItem params for the sale processing
 */
static void Parse_SaleItem(lua_State *L, ST_ITEM *pstItem)
{
	//local item ={"name"="Elma", "amount" = 1923, "kdv" =18, "Dept"=1,"unit" = 0, "count" = 1, "precisioncount"=0, "currency" = 949}

	memset(pstItem, 0x00, sizeof (ST_ITEM));

	lua_pushnil(L);
	while(lua_next(L, -2) != 0)
    {
        if(lua_isstring(L, -1))
		{
		  if (!memcmp(lua_tostring(L, -2), "name", 4))
		  {
			  strcpy(pstItem->name , lua_tostring(L, -1));
		  }
		  else if(!memcmp(lua_tostring(L, -2), "amount", 6))
		  {
			  pstItem->amount = (uint32)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "type", 4))
		  {
			  pstItem->type = (uint8)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "deptindex", strlen("deptindex")))
		  {
			  pstItem->deptIndex = (uint8)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "unit", 4))
		  {
			  pstItem->unitType = (uint8)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "count", 5))
		  {
			  pstItem->count = (uint16)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "precisioncount", 14))
		  {
			  pstItem->countPrecition = (uint8)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "currency", 8))
		  {
			  pstItem->currency = (uint16)lua_tonumber(L, -1);
		  }
		}
        else if(lua_isnumber(L, -1))
		{
          //printf("%s = %d\n", lua_tostring(L, -2), lua_tonumber(L, -1));
		}
        else if(lua_istable(L, -1))
		{
			break;
		}
        lua_pop(L, 1);
    }
}



/**
 *
 * @param L
 * @return
 */
int Lua_FiscalPrinter_TicketHeader(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;

	int n = lua_gettop(L);

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL || xHandle.retcode != TRAN_RESULT_OK)
		retcode = APP_ERR_GMP3_NO_HANDLE;
	else
	{
		// get number of arguments
		int i, TicketType = 0;
		for ( i = 1 ; i <= n ; ++i)
		{
			const char *p;
			 p = lua_typename(L, i);
			if (!lua_isnumber(L, i))
				continue;
			TicketType = (int)lua_tonumber(L, i);

			retcode = FiscalPrinter_TicketHeader((TTicketType)TicketType, 0);
		}
	}
	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;
}



/**
 *
 * @param L
 * @return
 */

int Lua_FiscalPrinter_Start(lua_State *L)
{
	unsigned short retcode = 0;

	ST_Handle xHandle;
	ST_Handle *pxHandle = NULL;

	xHandle.handle = 0;
	xHandle.pUniqueId = (char*)umalloc(24+1);
	memset(xHandle.pUniqueId, 0x00, 25);
	pxHandle = (ST_Handle *)lua_newuserdata(L, sizeof(xHandle));

	retcode = FiscalPrinter_Start((uint8*)xHandle.pUniqueId, 24, NULL, 0, NULL, 0, 0);
	xHandle.retcode = retcode;
	if (retcode== TRAN_RESULT_OK)
		xHandle.handle = (unsigned long)&pxHandle;
	// Assign it
	*pxHandle = xHandle;

	luaL_setmetatable(L, LUA_INTERNAL_NAME);
	// yukaridaki tek fonk da birlestirildi.
	//luaL_getmetatable(L, LUA_INTERNAL_NAME);
	//lua_setmetatable(L, -2);

	return 1;
}

int Lua_FiscalPrinter_SetInvoice(lua_State *L)
{
	unsigned short retcode = 0;
	ST_Handle *pxHandle;
	ST_TICKET m_stTicket;
	ST_INVIOCE_INFO stInvoiceInfo;
	int		n, i;

	memset(&m_stTicket, 0x00, sizeof(m_stTicket));

	pxHandle = check_Obj(L, 1);

	if (pxHandle == NULL)
		retcode = APP_ERR_GMP3_NO_HANDLE;

	n = lua_gettop(L);

	if (n != 2) // including userdata
		retcode = APP_ERR_MISSING_PARAMETER;

	if (retcode == 0)
	{
		memset(&stInvoiceInfo, 0x00, sizeof(stInvoiceInfo));
		for ( i = 1 ; i <= n ; ++i)
		{
			int t = lua_type(L, i);
			if (t == LUA_TUSERDATA)
				continue;

			if (lua_istable(L, i))
			{
				Parse_SetInvoice(L, &stInvoiceInfo);
			}
		}
		retcode = FiscalPrinter_SetInvoice( &stInvoiceInfo, &m_stTicket , 0);
	}

	//push the result code
	lua_pushinteger(L, retcode);

	/* return the number of results */
	return 1;


}

static void Parse_SetInvoice(lua_State *L, ST_INVIOCE_INFO *pstInvoiceInfo)
{
	//local invoice ={type= INVOICE_TYPE_PAPER, amount = 14500, currency = 949no ="123456789", tck="11111111111",vkn="",day = 13, month = 2, year=2017, flag = 0 }

	memset(pstInvoiceInfo, 0x00, sizeof (ST_INVIOCE_INFO));

	lua_pushnil(L);
	while(lua_next(L, -2) != 0)
    {
        if(lua_isstring(L, -1))
		{
          //printf("%s = %s\n", lua_tostring(L, -2), lua_tostring(L, -1));
		  if (!memcmp(lua_tostring(L, -2), "type", 4))
		  {
			  pstInvoiceInfo->source = lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "amount", 6))
		  {
			  pstInvoiceInfo->amount = (uint32)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "currency", 8))
		  {
			  pstInvoiceInfo->currency = (uint16)lua_tonumber(L, -1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "no", 2))
		  {
			  memcpy(pstInvoiceInfo->no, lua_tostring(L, -1), MINI(24, strlen(lua_tostring(L, -1))));
		  }
		  else if(!memcmp(lua_tostring(L, -2), "tck", 2))
		  {
			  memcpy(pstInvoiceInfo->tck_no, lua_tostring(L, -1), MINI(11, strlen(lua_tostring(L, -1))));
		  }
		  else if(!memcmp(lua_tostring(L, -2), "vkn", 2))
		  {
			  memcpy(pstInvoiceInfo->vk_no, lua_tostring(L, -1), MINI(11, strlen(lua_tostring(L, -1))));
		  }
		  else if(!memcmp(lua_tostring(L, -2), "day", 3))
		  {
			  int day = lua_tonumber(L, -1);
			  LongToBcd(day, (uint8*)&pstInvoiceInfo->date[2], 1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "month", 5))
		  {
			  int month = lua_tonumber(L, -1);
			  LongToBcd(month, (uint8*)&pstInvoiceInfo->date[1], 1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "year", 4))
		  {
			  int year = lua_tonumber(L, -1);
			  year %=2000;
			  LongToBcd(year, (uint8*)&pstInvoiceInfo->date[0], 1);
		  }
		  else if(!memcmp(lua_tostring(L, -2), "flag", 4))
		  {
			  pstInvoiceInfo->flag = lua_tonumber(L, -1);
		  }
		}
        else if(lua_isnumber(L, -1))
		{
          ;
		}
        else if(lua_istable(L, -1))
		{
			break;
		}
        lua_pop(L, 1);
    }
}


int luaopen_Fiscal(lua_State *L)
{
	static const luaL_Reg Fiscal_method[] =
	{
		{ "FiscalPrinter_TicketHeader"			, &Lua_FiscalPrinter_TicketHeader			},
		{ "FiscalPrinter_SetInvoice"			, &Lua_FiscalPrinter_SetInvoice				},
		{ "FiscalPrinter_ItemSale"				, &Lua_FiscalPrinter_ItemSale				},
		{ "FiscalPrinter_Payment"				, &Lua_FiscalPrinter_Payment				},
		{ "FiscalPrinter_PrintTotalsAndPayments", &Lua_FiscalPrinter_PrintTotalsAndPayments },
		{ "FiscalPrinter_PrintBeforeMF"			, &Lua_FiscalPrinter_PrintBeforeMF			},
		{ "FiscalPrinter_PrintMF"				, &Lua_FiscalPrinter_PrintMF				},
		{ "FiscalPrinter_Close"					, &Lua_FiscalPrinter_Close					},
		{ "FiscalPrinter_PrintUserMessage"		, &Lua_FiscalPrinter_PrintUserMessage		},
		{ "FiscalPrinter_OptionFlags"			, &Lua_FiscalPrinter_OptionFlags			},
		{ "FiscalPrinter_GetTicket"				, &Lua_FiscalPrinter_GetTicket				},
		{ "FiscalPrinter_Dec"					, &Lua_FiscalPrinter_Dec					},
		{ "FiscalPrinter_Inc"					, &Lua_FiscalPrinter_Inc					},
		{ "FiscalPrinter_Minus"					, &Lua_FiscalPrinter_Minus					},
		{ "FiscalPrinter_Pretotal"				, &Lua_FiscalPrinter_Pretotal				},
		{ "FiscalPrinter_VoidItem"				, &Lua_FiscalPrinter_VoidItem				},
		{ NULL, NULL }
	};

	static const luaL_Reg Fiscal_lib[] =
	{
		{ "FiscalPrinter_Start", &Lua_FiscalPrinter_Start },
		{ NULL, NULL }
	};

	luaL_newlib(L, Fiscal_lib);

	// Stack: MyLib
	luaL_newmetatable(L, LUA_INTERNAL_NAME); // Stack: MyLib meta
	luaL_newlib(L, Fiscal_method);
	lua_setfield(L, -2, "__index"); // Stack: MyLib meta

	lua_pushstring(L, "__gc");
	lua_pushcfunction(L, ingenico__gc); // Stack: MyLib meta "__gc" fptr
	lua_settable(L, -3); // Stack: MyLib meta
	lua_pop(L, 1); // Stack: MyLib

	return 1;
}

int Fiscal_LoadLibs(lua_State *L)
{
	luaL_requiref(L, LUA_FISCAL_LIBRARY_NAME, &luaopen_Fiscal, 1);
	lua_pop(L, 1); // requiref leaves the library table on the stack

	/* register our function */
	lua_register(L, "trace", trace);

	return 0;
}

