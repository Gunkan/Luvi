
/**
 *
 *! \file        Functions.c
 * \brief
 * \date	18 March 2017
 * \n This file contains utility function for formating buffer at the TLV format
 * \n
 * \note definition will be added later on
 * \link
 * \example
 * \author cemal.gunkan@ingenico.com
 * \copyright Ingenico TR (R&D).
**/

#include "string.h"
#include "luainc.h"
#include "GMPSmartDLL.h"

#define LUA_INGENICO_LIBRARY_NAME		"Ingenico"


int Lua_FiscalPrinter_Echo(lua_State *L)
{
	ST_ECHO stEcho;
	memset(&stEcho, 0x00, sizeof(stEcho));

	// �nce ECHO ile cihaz ile olan ba�lant�y� test et
	int retcode = 0; //FiscalPrinter_Echo( &stEcho, 0 );

	//push the result code
	lua_pushnumber(L, retcode);

	/* return the number of results */
	return 1;
}

int Lua_FiscalPrinter_GetTaxRates(lua_State *L)
{
	int retcode;
	uint8 numberOfTotalRecordsReceived;
	uint8 numberOfTotalTaxRates;
	ST_TAX_RATE		stTaxRates[8];

	memset( stTaxRates, 0x00, sizeof(stTaxRates));

	retcode = 0; // FiscalPrinter_GetTaxRates( &numberOfTotalTaxRates, &numberOfTotalRecordsReceived, stTaxRates, 8 ) ;

	//push the result code
	lua_pushnumber(L, retcode);
	/* return the number of results */
	return 1;
}
int Lua_FiscalPrinter_GetDepartments(lua_State *L)
{
	int retcode;
	uint8 numberOfTotalRecordsReceived;
	uint8 numberOfTotalDepartments;
	ST_DEPARTMENT	stDepartments[12+1];

	memset( stDepartments, 0x00, sizeof(stDepartments));

	retcode = 0;//FiscalPrinter_GetDepartments( &numberOfTotalDepartments, &numberOfTotalRecordsReceived, stDepartments, 12 );

	//push the result code
	lua_pushnumber(L, retcode);

	/* return the number of results */
	return 1;
}


int luaopen_Functions_Register(lua_State *L)
{
	static const luaL_Reg ingenico_function[] =
	{
		{ "FiscalPrinter_Echo"				, &Lua_FiscalPrinter_Echo },
		//{ "GMP_StartPairingInit"			, &Lua_GMP_StartPairingInit },
		//{ "FiscalPrinter_Ping"				, &Lua_FiscalPrinter_Ping },
		{ "FiscalPrinter_GetTaxRates"		, &Lua_FiscalPrinter_GetTaxRates },
		{ "FiscalPrinter_GetDepartments"	, &Lua_FiscalPrinter_GetDepartments },
		{ NULL, NULL }
	};

	luaL_newlib(L, ingenico_function);
	return 1;
}

int Functions_LoadLibs(lua_State *L)
{
	luaL_requiref(L, LUA_INGENICO_LIBRARY_NAME, &luaopen_Functions_Register, 1);
	lua_pop(L, 1); // requiref leaves the library table on the stack
	return 0;
}
